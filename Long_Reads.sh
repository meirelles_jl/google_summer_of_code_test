# Bash script used to generate assembly on .bam files with StringTie v2,
# and running Tablemaker to import data in R and extracting FPKM from assembly output
# To use it, you need to copy the Long_Reads.sh to a directory with the .bam files
# Usage: sh Long_Reads.sh


for file in *.bam; do					# Loop to iterate in all .bam files

	new="${file%.bam}"					# Here, we extract the name of the file without the extension(.bam)
	

	echo "Running stringtie on $file file"								# Run stringtie on .bam file, using 8 cores generating .gtf assembled data
	stringtie  -p 8 -L -o $new.gtf $file								# on long read mode

	echo "$new.gtf" >> mergelist.txt								# Save .gtf name in merged list, to use it later

	head -n 1000 $new.gtf > $new.1000.gtf								# Extract 1000 first lines to other file
done

echo "Running stringtie merge on files creating non-redundant set of transcripts"			# Running merge to differential analysis

stringtie --merge -p 8 -o merged.gtf mergelist.txt

for file in *bam; do
	
	new="${file%.bam}"                                      # Here, we extract the name of the file without the extension(.bam) again

	echo "Running tablemaker on $new.gtf file based on merged non-redundant gtf"
	tablemaker -p 8 --library-type fr-firststrand -q -W -G merged.gtf -o sample_$new $file		# Run tablemaker, using 8 cores and library type first strand
													# because the sequencing method used was TruSeq Stranded mRNA Sample Prep Kit
done	

echo "Running Ballgown R script for analysis of FPKM of each sample in each transcript"			# Run R script that creates FPKM matrix to each transcrip in each sample
Rscript Ballgown_script.R										

head -n 1000 FPKM.csv > FPKM_1000.csv									# Extract 1000 frist lines to other file
